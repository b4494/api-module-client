<?php

namespace BmPlatform\ApiModuleClient;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Support\Http\HttpClient;
use Psr\Http\Message\ResponseInterface;

class ApiClient extends HttpClient
{
    public function __construct(string $baseUrl, string $integrationCode, string $apiToken, array $options = [])
    {
        $options['base_uri'] = rtrim($baseUrl, '/').'/'.trim($integrationCode, '/').'/';
        $options['headers']['Authorization'] = "Bearer $apiToken";
        $options['headers']['Accept'] = 'application/json';

        parent::__construct($options);
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    protected function processResponse(ResponseInterface $response): mixed
    {
        return match (true) {
            $response->getStatusCode() == 500 => throw new ErrorException(ErrorCode::InternalServerError),
            $response->getStatusCode() == 502 => throw new ErrorException(ErrorCode::ServiceUnavailable),
            !($body = $response->getBody()->getContents()) && $response->getStatusCode() < 300 => true,
            !$body => throw new ErrorException(ErrorCode::UnexpectedServerError, $response->getStatusCode()),
            default => tap(json_decode($body, true, JSON_THROW_ON_ERROR), static function ($data) {
                if (isset($data['code'])) {
                    throw new ErrorException(ErrorCode::from($data['code']), $data['message']);
                }
            }),
        };
    }
}