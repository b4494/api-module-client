<?php

namespace BmPlatform\ApiModuleClient;

use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\ApiModuleClient\Events\Event;

class ApiCommands
{
    public function __construct(protected readonly ApiClient $client)
    {
        //
    }

    /** @return string Web url to where user can be redirected */
    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function integrate(AppIntegrationData $data): string
    {
        return $this->client->post('', [ 'json' => $data->toArray() ])['webUrl'];
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function postEvent(string $appId, Event $event): void
    {
        $this->client->post($appId.'/event', [
            'json' => $event->toArray(),
        ]);
    }
}