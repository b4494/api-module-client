<?php

namespace BmPlatform\ApiModuleClient\Events;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class Event implements \JsonSerializable, Arrayable
{
    public function __construct(public readonly ?Carbon $timestamp = null)
    {
        //
    }

    private static function serializeValue(mixed $value)
    {
        return match (true) {
            $value instanceof Arrayable => $value->toArray(),
            $value instanceof \DateTime => $value->format(\DateTime::RFC3339_EXTENDED),
            default => $value,
        };
    }

    public function toArray()
    {
        $refl = new \ReflectionClass($this);


        return collect($refl->getProperties(\ReflectionProperty::IS_PUBLIC))->mapWithKeys(fn (\ReflectionProperty $prop) => [
            $prop->getName() => static::serializeValue($prop->getValue($this)),
        ])->all();
    }

    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }
}