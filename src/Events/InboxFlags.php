<?php

namespace BmPlatform\ApiModuleClient\Events;

use Illuminate\Contracts\Support\Arrayable;

class InboxFlags implements \JsonSerializable, Arrayable
{
    public function __construct(
        public readonly bool $newTicketOpened = false,
        public readonly bool $newChatCreated = false,
        public readonly bool $externalPostComment = false
    ) {
        //
    }

    public function toArray()
    {
        return (array)$this;
    }

    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }
}