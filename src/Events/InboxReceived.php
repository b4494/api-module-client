<?php

namespace BmPlatform\ApiModuleClient\Events;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\ExternalPlatformItem;
use BmPlatform\Abstraction\DataTypes\MessageData;
use Carbon\Carbon;

class InboxReceived extends Event
{
    public function __construct(
        public readonly Chat|string           $chat,
        public readonly Contact|string        $participant,
        public readonly MessageData           $message,
        public readonly ?ExternalPlatformItem $externalItem = null,
        public readonly ?InboxFlags           $flags = null,
        ?Carbon                               $timestamp = null,
    ) {
        parent::__construct($timestamp);
    }
}