<?php

class EventsTest extends \PHPUnit\Framework\TestCase
{
    public function testInboxReceived()
    {
        $event = new \BmPlatform\ApiModuleClient\Events\InboxReceived(
            chat: 'chat',
            participant: 'participant',
            message: new \BmPlatform\Abstraction\DataTypes\MessageData(
                externalId: 'message',
                text: 'text'
            ),
            flags: new \BmPlatform\ApiModuleClient\Events\InboxFlags(newChatCreated: true),
            timestamp: $date = Carbon\Carbon::parse('2022-12-08'),
        );

        $this->assertEquals([
            'chat' => 'chat',
            'participant' => 'participant',
            'message' => [
                'externalId' => 'message',
                'text' => 'text',
                'attachments' => null,
                'geoLocation' => null,
                'date' => null,
                'extraData' => null,
            ],
            'flags' => [
                'newChatCreated' => true,
                'newTicketOpened' => false,
                'externalPostComment' => false,
            ],
            'timestamp' => '2022-12-08T00:00:00.000+00:00',
            'externalItem' => null,
        ], $event->toArray());
    }
}